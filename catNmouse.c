///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Jared Inouye <jinouye7@hawaii.edu>
/// @date    20_Jan_2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int DEFAULT_MAX_NUMBER = 2048;

int main( int argc, char* argv[] ) {
   printf( "Cat `n Mouse\n" );
   //printf( "The number of arguments is: %d\n", argc );
   
   //get the max value
   int theMaxValue;
   if (argc > 1){
      //printf("default overriden\n");
      theMaxValue = atoi(argv[1]);
      if (theMaxValue < 1){
         printf("value must be greater than or equal to 1\n");
         return 1;
         }
   }
   else{
      theMaxValue = DEFAULT_MAX_NUMBER;
   }
   //initialize random number generator
   srand((unsigned) time(NULL));
   //generate random value from 1 to the max value
   int num;
   num = rand() % theMaxValue +1;
   //printf("the answer is %d\n", num);
   
   
   do {

      //get user's guess
      printf("OK cat, I'm thinking of a number from 1 to %d. Make a guess:  ", theMaxValue);
      int Guess;
      scanf( "%d", &Guess );
      //printf( "The number was [%d]\n", Guess );

      if (Guess < 1) {
         printf("You must enter a number that’s >= 1\n");
         continue;
      }
      else if (Guess > theMaxValue){
         printf("You must enter a number that’s <= %d\n",theMaxValue);
         continue;
      }
      else if (Guess == num){
         break;
      }
      else if (num < Guess){
         printf("No cat... the number I’m thinking of is smaller than %d\n", Guess);
         continue;
      }
      else if (num > Guess) {
         printf("No cat... the number I’m thinking of is larger than %d\n", Guess);
         continue;
      }
   } while(1);

   
   printf("You got me\n");
   printf(" /\\_/\\\n( o.o )\n > ^ <\n");

   return 0;
}

